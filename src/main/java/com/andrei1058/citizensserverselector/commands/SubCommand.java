package com.andrei1058.citizensserverselector.commands;

import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;

public abstract class SubCommand {

    /* SubCommand name */
    private String name;
    /* Show the subCommand in the main command list*/
    private boolean show = false;
    /* SubCommand parent */
    private ParentCommand parent;
    /* Command order in main command list*/
    private int priority = 20;
    /* Display name/ info in subCommands list*/
    private TextComponent displayInfo;

    /**
     * Create a sub-command
     * Make sure you return true or it will say command not found
     *
     * @param name   sub-command name
     * @param parent parent command
     */
    public SubCommand(ParentCommand parent, String name) {
        this.name = name;
        this.parent = parent;
        parent.addSubCommand(this);
    }

    /**
     * Add your sub-command code under this method
     */
    public abstract boolean execute(String[] args, Player s);

    /**
     * Get sub-command name
     */
    public String getSubCommandName() {
        return name;
    }

    /**
     * Display the sub-command under the main command
     * Don't forget to set the displayInfo
     * Ops only will see it cuz players receive a commands list from messages file
     */
    public void showInList(boolean value) {
        this.show = value;
    }

    /**
     * This is the command information in the subCommands list of the target parent
     */
    public void setDisplayInfo(TextComponent displayInfo) {
        this.displayInfo = displayInfo;
    }

    /**
     * This is the command priority in the sub-commands list
     * You may use this method if you set showInList true
     * Commands with a minor number will be displayed first
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * Get parent command
     */
    public ParentCommand getParent() {
        return parent;
    }

    /**
     * Get show priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * Get command description for subCommands list
     */
    public TextComponent getDisplayInfo() {
        return displayInfo;
    }


    /**
     * Check if is displayed on the list
     */
    public boolean isShow() {
        return show;
    }
}
