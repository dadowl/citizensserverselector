package com.andrei1058.citizensserverselector.commands.main;

import com.andrei1058.citizensserverselector.commands.ParentCommand;
import com.andrei1058.citizensserverselector.commands.Permission;
import com.andrei1058.citizensserverselector.commands.SubCommand;
import com.andrei1058.citizensserverselector.Main;
import com.andrei1058.citizensserverselector.utils.CitizensUtils;
import com.andrei1058.citizensserverselector.utils.SpawnedNPC;
import net.citizensnpcs.api.npc.NPC;
import net.md_5.bungee.api.chat.ClickEvent;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RenameSubCmd extends SubCommand {
    /**
     * Create a sub-command
     * Make sure you return true or it will say command not found
     *
     * @param p    parent command
     * @param name sub-command name
     */
    @SuppressWarnings("WeakerAccess")
    public RenameSubCmd(ParentCommand p, String name) {
        super(p, name);
        setPriority(2);

        setDisplayInfo(MainCommand.createTC("§9" + MainCommand.getDot() + " §7/" + p.getName() + " " + getSubCommandName() + " §f- Rename an existing npc.",
                "/" + p.getName() + " " + getSubCommandName() + " ",
                "§fLook at an NPC and use this command in\n§forder to change the first hologram on its head.", ClickEvent.Action.SUGGEST_COMMAND));
        showInList(true);
    }

    @Override
    public boolean execute(String[] args, Player p) {

        if (!p.hasPermission(Permission.ALL_COMMANDS))return false;

        if (args.length < 1) {
            p.spigot().sendMessage(MainCommand.createTC("§9Usage: §7/css " + getSubCommandName() + " <name>",
                    "/css " + getSubCommandName() + " ", "§fLook at an NPC and use this command in" +
                            "\n§forder to change the first hologram on its head.", ClickEvent.Action.SUGGEST_COMMAND));
            return true;
        }

        StringBuilder displayName = new StringBuilder();
        String configPath;
        for (String arg : args) {
            displayName.append(arg).append(" ");
        }
        displayName = new StringBuilder(displayName.substring(0, displayName.length() - 1));
        configPath = displayName.toString().replace("&", "").replace(" ", "");


        Pattern pat = Pattern.compile("[^A-Za-z0-9]");
        Matcher m = pat.matcher(configPath);
        if (m.find()){
            p.sendMessage("§9Error: §7The name can contain numbers and letters only.");
            return true;
        }

        if (Main.getStorage().exists(configPath)) {
            p.sendMessage("§9Error: §7There is already set an NPC with this name!");
            return true;
        }

        NPC npc = CitizensUtils.getTarget(p);
        if (npc == null) {
            p.sendMessage("§9Error: §7You must look at a NPC in order to use this command.");
            return true;
        }

        SpawnedNPC sNPC = SpawnedNPC.getNpcById().get(npc.getId());
        if (sNPC == null) {
            p.sendMessage("§9Error: §7The target NPC is not managed by this plugin.");
            return true;
        }

        sNPC.rename(displayName.toString(), configPath);
        p.sendMessage("§9NPC renamed to " + ChatColor.translateAlternateColorCodes('&', displayName.toString()) + "§7.");
        return true;
    }
}
