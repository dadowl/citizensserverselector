package com.andrei1058.citizensserverselector.commands.main;

import com.andrei1058.citizensserverselector.Main;
import com.andrei1058.citizensserverselector.commands.ParentCommand;
import com.andrei1058.citizensserverselector.commands.Permission;
import com.andrei1058.citizensserverselector.commands.SubCommand;
import com.andrei1058.citizensserverselector.configuration.ConfigPath;
import com.andrei1058.citizensserverselector.utils.CitizensUtils;
import net.citizensnpcs.api.npc.NPC;
import net.md_5.bungee.api.chat.ClickEvent;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreateSubCmd extends SubCommand {
    /**
     * Create a sub-command
     * Make sure you return true or it will say command not found
     *
     * @param p    parent command
     * @param name sub-command name
     */
    @SuppressWarnings("WeakerAccess")
    public CreateSubCmd(ParentCommand p, String name) {
        super(p, name);
        setPriority(1);

        setDisplayInfo(MainCommand.createTC("§9" + MainCommand.getDot() + " §7/" + p.getName() + " " + getSubCommandName() + " §f- Create a new server npc.",
                "/" + p.getName() + " " + getSubCommandName() + " ",
                "§fCreate a new server selector npc.\n§fClick to view the command syntax.", ClickEvent.Action.SUGGEST_COMMAND));
        showInList(true);
    }

    @Override
    public boolean execute(String[] args, Player p) {

        if (!p.hasPermission(Permission.ALL_COMMANDS)) return false;

        if (args.length < 1) {
            p.spigot().sendMessage(MainCommand.createTC("§9Usage: §7/css " + getSubCommandName() + " <name>",
                    "/css " + getSubCommandName() + " ", "§fCreate a NPC with given name.", ClickEvent.Action.SUGGEST_COMMAND));
            return true;
        }
        StringBuilder displayName = new StringBuilder();
        String configPath;
        for (String arg : args) {
            displayName.append(arg).append(" ");
        }
        displayName = new StringBuilder(displayName.substring(0, displayName.length() - 1));
        configPath = displayName.toString().replace("&", "").replace(" ", "");

        if (Main.getStorage().exists(configPath)) {
            p.sendMessage("§9Error: §7There is already set an NPC with this name!");
            return true;
        }

        Pattern pat = Pattern.compile("[^A-Za-z0-9]");
        Matcher m = pat.matcher(configPath);
        if (m.find()) {
            p.sendMessage("§9Error: §7The name can contain numbers and letters only.");
            return true;
        }

        NPC npc = CitizensUtils.spawnNPC(p.getLocation(), displayName.toString(), "", configPath, "Steve", null);

        Main.getStorage().set(ConfigPath.NPC_DISPLAYNAME.replace("%key%", configPath), displayName.toString());
        Main.getStorage().set(ConfigPath.NPC_LOCATION.replace("%key%", configPath), Main.getStorage().locationToString(p.getLocation()));
        Main.getStorage().set(ConfigPath.NPC_ID.replace("%key%", configPath), npc.getId());

        p.sendMessage("§9" + MainCommand.getDot() + " " + ChatColor.translateAlternateColorCodes('&', displayName.toString()) + " §7was created!");

        return true;
    }
}
