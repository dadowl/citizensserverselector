package com.andrei1058.citizensserverselector.pluginsupport;

import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.entity.Player;

public class PAPI {

    public interface PAPISupport {

        String replacePlaceholders(Player player, String message);
    }

    public static class WithPAPI implements PAPISupport {

        @Override
        public String replacePlaceholders(Player player, String message) {
            return PlaceholderAPI.setPlaceholders(player, message);
        }
    }

    public static class WithoutPAPI implements PAPISupport {

        @Override
        public String replacePlaceholders(Player player, String message) {
            return message;
        }
    }
}
