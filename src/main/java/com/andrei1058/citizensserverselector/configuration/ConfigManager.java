package com.andrei1058.citizensserverselector.configuration;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class ConfigManager {

    private YamlConfiguration yml;
    private File config;
    private boolean firstTime = false;

    public ConfigManager(String name, String dir) {
        File d = new File(dir);
        if (!d.exists()) {
            //noinspection ResultOfMethodCallIgnored
            d.mkdir();
            firstTime = true;
        }
        config = new File(dir + "/" + name + ".yml");
        if (!config.exists()) {
            firstTime = true;
            try {
                //noinspection ResultOfMethodCallIgnored
                config.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        yml = YamlConfiguration.loadConfiguration(config);
    }

    /** Reload file */
    public void reload() {
        yml = YamlConfiguration.loadConfiguration(config);
    }

    /** Convert a location to a string */
    public String locationToString(Location loc) {
        if (loc.getWorld() == null) return null;
        return loc.getX() + "," + loc.getY() + "," + loc.getZ() + "," + ((double)loc.getYaw()) + "," + ((double)loc.getPitch()) + "," + loc.getWorld().getName();
    }


    /** Save a location as string in config*/
    public void saveLocation(String path, Location loc) {
        yml.set(path, locationToString(loc));
        save();
    }

    /** Get a location from path*/
    public Location getLocation(String path) {
        String d = yml.getString(path);
        //noinspection ConstantConditions
        String[] data = d.replace("[", "").replace("]", "").split(",");
        return new Location(Bukkit.getWorld(data[5]), Double.parseDouble(data[0]), Double.parseDouble(data[1]), Double.parseDouble(data[2]), Float.parseFloat(data[3]), Float.parseFloat(data[4]));
    }

    /** Add a new value to config */
    public void set(String path, Object value) {
        yml.set(path, value);
        save();
    }

    /** Get yml instance */
    public YamlConfiguration getYml() {
        return yml;
    }

    /** Save changes */
    public void save() {
        try {
            yml.save(config);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** Get a boolean from yml */
    public boolean getBoolean(String path) {
        return yml.getBoolean(path);
    }

    /** Get int from yml*/
    public int getInt(String path) {
        return yml.getInt(path);
    }

    /** Get a string from yml */
    public String getString(String path) {
        return yml.getString(path);
    }

    /** Check if the file was just created */
    public boolean isFirstTime() {
        return firstTime;
    }

    /** Return true if locations are the same */
    public boolean compareLocations(Location l1, Location l2){
        return l1.getBlockX() == l2.getBlockX() && l1.getBlockZ() == l2.getBlockZ() && l1.getBlockY() == l2.getBlockY() && l1.getWorld() == l2.getWorld();
    }

    /** Check if path exists */
    public boolean exists(String path){
        return yml.get(path) != null;
    }
}
