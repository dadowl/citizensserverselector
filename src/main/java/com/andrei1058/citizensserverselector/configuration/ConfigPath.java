package com.andrei1058.citizensserverselector.configuration;

public class ConfigPath {

    public static final String NPC_DISPLAYNAME = "%key%.name-line";
    public static final String NPC_SECOND_LINE = "%key%.counter-line";
    public static final String NPC_SKIN_NAME = "%key%.skin-name";
    public static final String NPC_COUNTER_TARGET = "%key%.target-counter";
    public static final String NPC_COUNTER_TYPE = "%key%.counter-type";
    public static final String NPC_ID = "%key%.citizens-id";
    public static final String NPC_LOCATION = "%key%.location";
    public static final String NPC_COMMAND = "%key%.command";

    public static final String CONFIG_SERVER_NAME = "server-name";

}
