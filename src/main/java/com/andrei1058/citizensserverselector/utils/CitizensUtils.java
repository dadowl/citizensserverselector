package com.andrei1058.citizensserverselector.utils;

import com.andrei1058.citizensserverselector.Main;
import com.andrei1058.citizensserverselector.configuration.ConfigPath;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.npc.skin.SkinnableEntity;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.util.BlockIterator;

public class CitizensUtils {

    /**
     * Get target NPC
     */
    public static net.citizensnpcs.api.npc.NPC getTarget(Player player) {
        BlockIterator iterator = new BlockIterator(player.getWorld(), player.getLocation().toVector(), player.getEyeLocation().getDirection(), 0, 100);
        while (iterator.hasNext()) {
            Block item = iterator.next();
            for (Entity entity : player.getNearbyEntities(100, 100, 100)) {
                int acc = 2;
                for (int x = -acc; x < acc; x++) {
                    for (int z = -acc; z < acc; z++) {
                        for (int y = -acc; y < acc; y++) {
                            if (entity.getLocation().getBlock().getRelative(x, y, z).equals(item)) {
                                if (entity.hasMetadata("NPC")) {
                                    net.citizensnpcs.api.npc.NPC npc = CitizensAPI.getNPCRegistry().getNPC(entity);
                                    if (npc != null) return npc;
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * Spawn a join-NPC
     *
     * @param l    Location where to be spawned
     * @param name Display name
     * @param skin A player name to get his skin
     */
    public static NPC spawnNPC(Location l, String name, String secondLine, String configKey, String skin, NPC spawnExisting) {
        NPC npc;
        if (spawnExisting == null) {
            npc = CitizensAPI.getNPCRegistry().createNPC(EntityType.PLAYER, "");
        } else {
            npc = spawnExisting;
        }
        if (!npc.isSpawned()) {
            npc.spawn(l);
        }
        if (npc.getEntity() instanceof SkinnableEntity) ((SkinnableEntity) npc.getEntity()).setSkinName(skin);
        npc.setProtected(true);
        npc.setName("");
        //noinspection ConstantConditions
        for (Entity e : l.getWorld().getNearbyEntities(l, 1, 3, 1)) {
            if (e.getType() == EntityType.ARMOR_STAND) {
                if (!((ArmorStand) e).isVisible()) {
                    e.remove();
                }
            }
        }
        ArmorStand a = createArmorStand(l.clone().add(0, 0.05, 0));
        a.setMarker(false);
        a.setCustomNameVisible(true);
        a.setCustomName(ChatColor.translateAlternateColorCodes('&', name.replace("{players}", "0")));
        ArmorStand a2 = null;
        if (!secondLine.isEmpty()) {
            a2 = createArmorStand(l.clone().subtract(0, 0.25, 0));
            a2.setMarker(false);
            a2.setCustomName(ChatColor.translateAlternateColorCodes('&', secondLine.replace("{players}", "0")));
            a2.setCustomNameVisible(true);
        }
        npc.teleport(l, PlayerTeleportEvent.TeleportCause.PLUGIN);
        npc.setName("");
        new SpawnedNPC(configKey, a, a2, npc);
        return npc;
    }

    /**
     * Create armor stand at location
     */
    @SuppressWarnings({"WeakerAccess", "ConstantConditions"})
    public static ArmorStand createArmorStand(Location loc) {
        ArmorStand a = loc.getWorld().spawn(loc, ArmorStand.class);
        a.setGravity(false);
        a.setVisible(false);
        a.setCustomNameVisible(false);
        a.setMarker(true);
        return a;
    }

    /**
     * Load existing NPCs from config
     */
    @SuppressWarnings("ConstantConditions")
    public static void loadExisting() {
        for (String key : Main.getStorage().getYml().getConfigurationSection("").getKeys(false)) {
            if (key.isEmpty()) continue;
            if (!Main.getStorage().exists(ConfigPath.NPC_LOCATION.replace("%key%", key))) {
                Main.getInstance().getLogger().severe("Could not load NPC: " + key);
                Main.getInstance().getLogger().severe("Location not set!");
                Main.getInstance().getLogger().severe("Removing from storage.");
                Main.getStorage().set(key, null);
                continue;
            }
            if (!Main.getStorage().exists(ConfigPath.NPC_ID.replace("%key%", key))) {
                Main.getInstance().getLogger().severe("Citizens ID missing for NPC: " + key);
                Main.getInstance().getLogger().severe("Removing from storage.");
                Main.getStorage().set(key, null);
                continue;
            }
            if (!Main.getStorage().exists(ConfigPath.NPC_DISPLAYNAME.replace("%key%", key))) {
                Main.getInstance().getLogger().severe("Display name missing for NPC: " + key);
                Main.getInstance().getLogger().severe("Removing from storage.");
                Main.getStorage().set(key, null);
                continue;
            }
            NPC npc = CitizensAPI.getNPCRegistry().getById(Main.getStorage().getInt(ConfigPath.NPC_ID.replace("%key%", key)));
            if (npc == null) {
                Main.getInstance().getLogger().severe("Something went wrong with Citizens when spawning NPC: " + key);
                continue;
            }
            String secondLine = "", skin = "Steve";
            if (Main.getStorage().exists(ConfigPath.NPC_SECOND_LINE.replace("%key%", key))) {
                secondLine = Main.getStorage().getString(ConfigPath.NPC_SECOND_LINE.replace("%key%", key));
            }

            if (Main.getStorage().exists(ConfigPath.NPC_SKIN_NAME.replace("%key%", key))) {
                skin = Main.getStorage().getString(ConfigPath.NPC_SKIN_NAME.replace("%key%", key));
            }

            spawnNPC(Main.getStorage().getLocation(ConfigPath.NPC_LOCATION.replace("%key%", key)),
                    Main.getStorage().getString(ConfigPath.NPC_DISPLAYNAME.replace("%key%", key)),
                    secondLine, key, skin, npc);
        }
    }
}
