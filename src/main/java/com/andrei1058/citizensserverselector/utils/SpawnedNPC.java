package com.andrei1058.citizensserverselector.utils;

import com.andrei1058.citizensserverselector.Main;
import com.andrei1058.citizensserverselector.configuration.ConfigManager;
import com.andrei1058.citizensserverselector.configuration.ConfigPath;
import com.andrei1058.citizensserverselector.updater.PlayerCounter;
import net.citizensnpcs.api.event.DespawnReason;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.npc.skin.SkinnableEntity;
import org.bukkit.ChatColor;
import org.bukkit.entity.ArmorStand;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SpawnedNPC {

    private static HashMap<Integer, SpawnedNPC> npcById = new HashMap<>();
    private static List<SpawnedNPC> npcList = new ArrayList<>();

    private ArmorStand firstLine;
    private String pathName, command = "";
    private ArmorStand secondLine;
    private CounterType counterType = CounterType.SERVER;
    private NPC npc;
    private List<String> targetCounter = new ArrayList<>();
    private PlayerCounter playerCounter;

    public SpawnedNPC(String configKey, ArmorStand firstLine, ArmorStand secondLine, NPC npc) {
        this.firstLine = firstLine;
        this.secondLine = secondLine;
        this.pathName = configKey;
        this.npc = npc;

        if (Main.getStorage().getYml().get(ConfigPath.NPC_COUNTER_TYPE.replace("%key%", pathName)) != null) {
            //noinspection ConstantConditions
            this.counterType = CounterType.valueOf(Main.getStorage().getString(ConfigPath.NPC_COUNTER_TYPE.replace("%key%", pathName))) == null ?
                    CounterType.SERVER : CounterType.valueOf(Main.getStorage().getString(ConfigPath.NPC_COUNTER_TYPE.replace("%key%", pathName)));
        }

        if (Main.getStorage().getYml().get(ConfigPath.NPC_COMMAND.replace("%key%", pathName)) != null) {
            this.command = Main.getStorage().getString(ConfigPath.NPC_COMMAND.replace("%key%", pathName));
        }

        if (counterType == CounterType.SERVER && Main.getServersUpdater() == null) {
            firstLine.remove();
            secondLine.remove();
            npc.despawn(DespawnReason.PLUGIN);
            Main.getInstance().getLogger().severe("I can't spawn " + configKey + " because you didn't set bungeecord to true in spigot.yml");
            return;
        }

        playerCounter = counterType == CounterType.SERVER ? Main.getServersUpdater() : Main.getWorldsUpdater();

        if (Main.getStorage().getYml().get(ConfigPath.NPC_COUNTER_TARGET.replace("%key%", pathName)) != null) {
            String[] playerCounter = Main.getStorage().getString(ConfigPath.NPC_COUNTER_TARGET.replace("%key%", pathName)).split("/+");
            for (String s : playerCounter) {
                if (!new ArrayList<>(targetCounter).contains(s)) targetCounter.add(s);
                if (counterType == CounterType.SERVER) {
                    Main.getServersUpdater().addCounterKey(s);
                }
            }
        }

        npcById.put(npc.getId(), this);
        npcList.add(this);

        updatePlayerCounter();
    }

    /**
     * Update player counter line
     */
    public void updatePlayerCounter() {
        if (secondLine == null) return;
        int amount = 0;
        for (String s : targetCounter) {
            amount += playerCounter.getCounter(s);
        }
        secondLine.setCustomName(Main.getPapiSupport().replacePlaceholders(null, ChatColor.translateAlternateColorCodes('&',
                Main.getStorage().getString(ConfigPath.NPC_SECOND_LINE.replace("%key%", pathName))).replace("{players}", String.valueOf(amount))));
    }

    /**
     * Change the second line on the head
     */
    public void setSecondLine(String string) {
        Main.getStorage().set(ConfigPath.NPC_SECOND_LINE.replace("%key%", pathName), string);
        if (secondLine == null) {
            this.secondLine = CitizensUtils.createArmorStand(npc.getEntity().getLocation().clone().subtract(0, 0.25, 0));
            this.secondLine.setMarker(false);
        }
        updatePlayerCounter();
        this.secondLine.setCustomNameVisible(true);
    }

    /**
     * Rename
     */
    public void rename(String newName, String newPath) {
        if (firstLine == null) return;
        firstLine.setCustomName(ChatColor.translateAlternateColorCodes('&', newName));
        ConfigManager s = Main.getStorage();

        s.set(ConfigPath.NPC_DISPLAYNAME.replace("%key%", newPath), newName);
        s.set(ConfigPath.NPC_LOCATION.replace("%key%", newPath), s.getString(ConfigPath.NPC_LOCATION.replace("%key%", pathName)));
        s.set(ConfigPath.NPC_ID.replace("%key%", newPath), npc.getId());


        if (s.exists(ConfigPath.NPC_SECOND_LINE.replace("%key%", pathName))) {
            s.set(ConfigPath.NPC_SECOND_LINE.replace("%key%", newPath), s.getString(ConfigPath.NPC_SECOND_LINE.replace("%key%", pathName)));

        }
        if (s.exists(ConfigPath.NPC_COUNTER_TYPE.replace("%key%", pathName))) {
            s.set(ConfigPath.NPC_COUNTER_TYPE.replace("%key%", newPath), s.getString(ConfigPath.NPC_COUNTER_TYPE.replace("%key%", pathName)));
        }
        if (s.exists(ConfigPath.NPC_COMMAND.replace("%key%", pathName))) {
            s.set(ConfigPath.NPC_COMMAND.replace("%key%", newPath), s.getString(ConfigPath.NPC_COMMAND.replace("%key%", pathName)));
        }

        s.set(pathName, null);

        this.pathName = newPath;
    }

    /**
     * Set target counter
     */
    public void setPlayerCounter(String string) {
        String[] playerCounter = string.split("\\+");
        Main.getStorage().set(ConfigPath.NPC_COUNTER_TARGET.replace("%key%", pathName), string);
        targetCounter = new ArrayList<>();
        for (String s : playerCounter) {
            if (!new ArrayList<>(targetCounter).contains(s)) targetCounter.add(s);
            if (counterType == CounterType.SERVER) {
                Main.getServersUpdater().addCounterKey(s);
            }
        }
    }

    /**
     * Set counter type
     */
    public void setCounterType(CounterType counterType) {
        this.counterType = counterType;
        this.playerCounter = counterType == CounterType.SERVER ? Main.getServersUpdater() : Main.getWorldsUpdater();
        Main.getStorage().set(ConfigPath.NPC_COUNTER_TYPE.replace("%key%", pathName), counterType.toString());
        updatePlayerCounter();
    }

    /**
     * Get counter type
     */
    public CounterType getCounterType() {
        return counterType;
    }

    /**
     * Change skin
     */
    public void setSkin(String skin) {
        if (npc.getEntity() instanceof SkinnableEntity) ((SkinnableEntity) npc.getEntity()).setSkinName(skin);
        Main.getStorage().set(ConfigPath.NPC_SKIN_NAME.replace("%key%", pathName), skin);
    }

    /**
     * Get npc command
     */
    public String getCommand() {
        return command;
    }

    /**
     * Set command for npc
     */
    public void setCommand(String command) {
        this.command = command;
        Main.getStorage().set(ConfigPath.NPC_COMMAND.replace("%key%", pathName), command);
    }

    /**
     * Detele npc
     */
    public void delete(boolean event) {
        npcList.remove(this);
        npcById.remove(npc.getId());
        if (firstLine != null) firstLine.remove();
        if (secondLine != null) secondLine.remove();
        Main.getStorage().getYml().set(pathName, null);
        Main.getStorage().save();
        if (!event) npc.destroy();
    }

    /**
     * Get spawned NPCs list
     */
    public static List<SpawnedNPC> getNpcList() {
        return new ArrayList<>(npcList);
    }

    /**
     * Get an NPC by ID
     */
    public static HashMap<Integer, SpawnedNPC> getNpcById() {
        return new HashMap<>(npcById);
    }
}
