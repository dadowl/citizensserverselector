package com.andrei1058.citizensserverselector.listeners;

import com.andrei1058.citizensserverselector.Main;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoin implements Listener {

    @EventHandler
    // Update counters
    public void onJoin(PlayerJoinEvent e) {
        if (Main.getServersUpdater() != null) {
            if (Main.getServersUpdater().getLastUpdate() + 5000 < System.currentTimeMillis()) {
                Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> {
                    Main.getServersUpdater().init(e.getPlayer());
                    Main.getServersUpdater().updateHolograms();
                }, 30L);
            }
        }

        if (e.getPlayer().isOp() && Main.updateAvailable) {
            Player p = e.getPlayer();
            p.sendMessage("§8[§fCitizensServerSelector§8]§7§m---------------------------");
            p.sendMessage("");
            TextComponent tc = new TextComponent("§eThere is a new update available: §6" + Main.newVersion + " §7§o(click)");
            tc.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://www.spigotmc.org/resources/60898/"));
            p.spigot().sendMessage(tc);
            p.sendMessage("");
            p.sendMessage("§8[§ffCitizensServerSelector§8]§7§m---------------------------");
        }
    }
}
