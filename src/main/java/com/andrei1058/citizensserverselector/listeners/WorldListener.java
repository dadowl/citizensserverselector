package com.andrei1058.citizensserverselector.listeners;

import com.andrei1058.citizensserverselector.Main;
import com.andrei1058.citizensserverselector.utils.CounterType;
import com.andrei1058.citizensserverselector.utils.SpawnedNPC;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class WorldListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Main.getWorldsUpdater().update(e.getPlayer().getWorld().getName(), e.getPlayer().getWorld().getPlayers().size());
        for (SpawnedNPC npc : SpawnedNPC.getNpcList()) {
            if (npc.getCounterType() == CounterType.WORLD) {
                npc.updatePlayerCounter();
            }
        }
    }
}
